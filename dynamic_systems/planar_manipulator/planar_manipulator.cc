#include "dynamic_systems/planar_manipulator.h"

#define sq(x1) (x1*x1)

PlanarManipulator::PlanarManipulator() {
  /* Set inertia of manipulator levers (assumed to be a rod) */
  this->par.inertia_lever1_kgm2 = 1. / 12. * this->par.mass_lever1_kg * sq(this->par.length_lever1_m);
  this->par.inertia_lever2_kgm2 = 1. / 12. * this->par.mass_lever2_kg * sq(this->par.length_lever2_m);
}

/**
 * @brief Compute matrix analytically for 2 DOF planar manipulator
 * 
 * @return Mass matrix from `M(theta) * theta_ddot + C(theta_dot, theta) = T`
 */
std::vector<std::vector<double_t>> PlanarManipulator::GetMassMatrix() {
    double_t phi1 = this->state[1];

    /* Definition of shorthands for parameters */
    double_t J1 = this->par.inertia_lever1_kgm2;
    double_t J2 = this->par.inertia_lever2_kgm2;
    double_t m1 = this->par.mass_lever1_kg;
    double_t m2 = this->par.mass_lever2_kg;
    double_t L1 = this->par.length_lever1_m;
    double_t L2 = this->par.length_lever2_m;

    double_t M11 = J1 + J2 + 0.25 * m1 * sq(L1) + m2 * sq(L1) + m2 * L1 * L2 * cos(phi1) + 0.25 * m2 * sq(L2);
    double_t M12 = J2 + 0.5 * m2 * L1 * L2 * cos(phi1) + 0.25 * m2 * sq(L2);
    double_t M21 = M12;
    double_t M22 = J2 + 0.25 * m2 * sq(L2);

    return std::vector<std::vector<double_t>> {{M11, M21}, {M12, M22}};
}

/**
 * @brief Get system output by computing end-effector position and velocity
 * 
 * @return PlanarManipulator::Output
 */
PlanarManipulator::Output PlanarManipulator::GetOutput() const {
  /* Definition of shorthands for states and actions
   * phi0: Base joint angle
   * phi1: First joint angle
   * w0: Angular velocity of base joint
   * w1: Angular velocity of first joint
   */
  double_t phi0 = this->state[0];
  double_t phi1 = this->state[1];
  double_t w0 = this->state[2];
  double_t w1 = this->state[3];

  /* Compute x-y-position of first joint */
  double_t x0 = this->par.length_lever1_m * cos(phi0);
  double_t y0 = this->par.length_lever1_m * sin(phi0);

  /* Compute x-y-position of endeffector */
  double_t x1 = x0 + this->par.length_lever2_m * cos(phi0 + phi1);
  double_t y1 = y0 + this->par.length_lever2_m * sin(phi0 + phi1);

  /* Compute x-y-velocities of endeffector */
  double_t L1 = this->par.length_lever1_m;
  double_t L2 = this->par.length_lever2_m;
  double_t lam00 = -L1 * sin(phi0) - L2 * sin(phi0 + phi1);
  double_t lam01 = -L2 * sin(phi0 + phi1);
  double_t lam10 = L1 * cos(phi0) + L2 * cos(phi0 + phi1);
  double_t lam11 = L2 * cos(phi0 + phi1);
  double_t x_dot = lam00 * w0 + lam01 * w1;
  double_t y_dot = lam10 * w0 + lam11 * w1;
  return {x1, y1, x_dot, y_dot, x0, y0};
}

/**
 * @brief Evaluation of the planar manipulator's ODE right handside 
 * 
 * @param state phi0: Base angle
 *              phi1: Middle angle
 *              w0: Angular velocity base angle
 *              w1: Angular velocity middle angle
 * @param action t0: Base joint torque
 *               t1: Middle joint torque
 * @return PlanarManipulator::StateDerivative 
 */
PlanarManipulator::StateDerivative PlanarManipulator::Dynamics(State state, Action action) const {
  /* Definition of shorthands for states and actions */
  double_t t0 = action[0];
  double_t t1 = action[1];
  double_t phi0 = state[0];
  double_t phi1 = state[1];
  double_t w0 = state[2];
  double_t w1 = state[3];

  /* Definition of shorthands for parameters */
  double_t J1 = this->par.inertia_lever1_kgm2;
  double_t J2 = this->par.inertia_lever2_kgm2;
  double_t m1 = this->par.mass_lever1_kg;
  double_t m2 = this->par.mass_lever2_kg;
  double_t L1 = this->par.length_lever1_m;
  double_t L2 = this->par.length_lever2_m;
  double_t g = this->par.acceleration_gravity_mPerSecPerSec;

  /* Computation of matrices defining the system dynamics
   * M*[phi0_ddot; phi1_ddot] + ...
   * C(phi0, phi0, phi0_dot, phi1_dot) + ...
   * G(phi0, phi1) ...
   * = [t0; t1] 
   */
  double_t M11 = J1 + J2 + 0.25 * m1 * sq(L1) + m2 * sq(L1) + m2 * L1 * L2 * cos(phi1) + 0.25 * m2 * sq(L2);
  double_t M12 = J2 + 0.5 * m2 * L1 * L2 * cos(phi1) + 0.25 * m2 * sq(L2);
  double_t M21 = M12;
  double_t M22 = J2 + 0.25 * m2 * sq(L2);
  double_t C0 = -m2 * L1 * L2 * sin(phi1) * w0 * w1 - 0.5 * m2 * L1 * L2 * sin(phi1) * sq(w1);
  double_t C1 = -0.5 * m2 * L1 * L2 * sin(phi1) * sq(w0);
  double_t G0 = g * (0.5 * m1 * L1 * cos(phi0) + m2 * L1 * cos(phi0) + 0.5 * m2 * L2 * cos(phi0 + phi1));
  double_t G1 = 0.5 * g * m2 * L2 * cos(phi0 + phi1);

  /* Computation mass matrix inverse */
  double_t det_M = M11 * M22 - M12 * M21;
  double_t inv_M00 = M22 / det_M;
  double_t inv_M01 = -M12 / det_M;
  double_t inv_M10 = -M21 / det_M;
  double_t inv_M11 = M11 / det_M;

  /* Computation state derivatives */
  double_t phi0_dot = w0;
  double_t phi1_dot = w1;
  double_t phi0_ddot = inv_M00 * (t0 - C0 - G0) + inv_M01 * (t1 - C1 - G1);
  double_t phi1_ddot = inv_M10 * (t0 - C0 - G0) + inv_M11 * (t1 - C1 - G1);
  return PlanarManipulator::StateDerivative {phi0_dot, phi1_dot, phi0_ddot, phi1_ddot};
}
