#pragma once
#include "dynamic_systems/dynamic_systems.h"

class PlanarDrone : public dynamic_systems::DynamicSystem<6, 2, 6> {

private:
    struct Parameters {
        double_t mass = 0.55; /* Drone mass [kg] */
        double_t gravity = 9.81; /* Earth acceleration [m*s^-2] */
        double_t arm_length = 0.086; /* Distance to rotor from center point [m] */
        double_t inertia = 2.320e-04; /* Inertia [kg*m^2] */
    } par;
    StateDerivative Dynamics(State state, Action action) const;

};
