#pragma once
#include <vector>
#include "dynamic_systems/dynamic_systems.h"

class PlanarManipulator : public dynamic_systems::DynamicSystem<4, 2, 6> {

public:
    PlanarManipulator();
    struct Parameters {
        double_t mass_lever1_kg = 0.55;
        double_t mass_lever2_kg = 0.55;
        double_t length_lever1_m = 1;
        double_t length_lever2_m = .8;
        double_t acceleration_gravity_mPerSecPerSec = 9.81;
        double_t inertia_lever1_kgm2;
        double_t inertia_lever2_kgm2;
    } par;
    Output GetOutput() const;
    std::vector<std::vector<double_t>> GetMassMatrix();

private:
    StateDerivative Dynamics(State state, Action action) const;

};
