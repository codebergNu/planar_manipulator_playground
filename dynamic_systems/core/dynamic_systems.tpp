namespace dynamic_systems {

template<size_t n_states_t, size_t n_actions_t, size_t n_outputs_t>
typename DynamicSystem<n_states_t, n_actions_t, n_outputs_t>::State DynamicSystem<n_states_t, n_actions_t, n_outputs_t>::GetState() const {
    return state;
}

template<size_t n_states_t, size_t n_actions_t, size_t n_outputs_t>
double_t DynamicSystem<n_states_t, n_actions_t, n_outputs_t>::GetStepSize() const {
    return this->step_size;
}

template<size_t n_states_t, size_t n_actions_t, size_t n_outputs_t>
void DynamicSystem<n_states_t, n_actions_t, n_outputs_t>::SetState(State state) {
    for (unsigned int ii=0; ii<state.size(); ii++) {
        this->state[ii] = state[ii];
    }
}

/* ERK4 */
template<size_t n_states_t, size_t n_actions_t, size_t n_outputs_t>
void DynamicSystem<n_states_t, n_actions_t, n_outputs_t>::SimulateStep(Action action) {
    State state_sim = state;
    State k1 = this->Dynamics(state_sim, action);
    State k2 = this->Dynamics(state_sim + k1 * (this->step_size / 2.), action);
    State k3 = this->Dynamics(state_sim + k2 * (this->step_size / 2.), action);
    State k4 = this->Dynamics(state_sim + k3 * this->step_size, action);
    state = state + (k1 + k2 * 2. + k3 * 2. + k4) * (this->step_size / 6.);
}

} /* namespace dynamic_systems */
