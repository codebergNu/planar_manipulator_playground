/* Print state vector */
template<unsigned long n_states_t>
std::ostream& operator<<(std::ostream& out, const std::array<double_t, n_states_t>& state) {
    out << "[";
    for (unsigned int ii = 0; ii < state.size() - 1; ii++) {
        out << state[ii] << ", ";
    }
    out << state[state.size() - 1] << "]";
    return out;
}

/* Sum of two state vectors */
template<size_t n_states_t>
std::array<double_t, n_states_t> operator+(const std::array<double_t, n_states_t>& left, const std::array<double_t, n_states_t>& right) {
    std::array<double_t, n_states_t> result;
    for (unsigned int ii = 0; ii < left.size(); ii++) {
        result[ii] = left[ii] + right[ii];
    }
    return result;
}

/* Product of state vector with scalar */
template<size_t n_states_t>
std::array<double_t, n_states_t> operator*(const std::array<double_t, n_states_t>& state, double_t factor) {
    std::array<double_t, n_states_t> result;
    for (unsigned int ii = 0; ii < state.size(); ii++) {
        result[ii] = state[ii] * factor;
    }
    return result;
}
