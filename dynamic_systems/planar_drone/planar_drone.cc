#include "dynamic_systems/planar_drone.h"

PlanarDrone::StateDerivative PlanarDrone::Dynamics(State state, Action action) const {
  double_t F1 = action[0];
  double_t F2 = action[1];
  double_t phi = state[2];
  double_t x_dot = state[3];
  double_t y_dot = state[4];
  double_t phi_dot = state[5];
  double_t x_ddot = 1 / par.mass * sin(phi) * (F1 + F2);
  double_t y_ddot =  1 / par.mass * cos(phi) * (F1 + F2) - par.gravity;
  double_t phi_ddot = 1 / par.inertia * (F1 - F2) * par.arm_length;
  return PlanarDrone::StateDerivative {x_dot, y_dot, phi_dot, x_ddot, y_ddot, phi_ddot};
}
