#pragma once
#include <iostream>
#include <array>
#include <cmath>

namespace dynamic_systems {

/* Dynamic systems class */
template<size_t n_states_t, size_t n_actions_t, size_t n_outputs_t>
class DynamicSystem {

    public: 
        int n_states = (int) n_states_t;
        int n_actions = (int) n_actions_t;
        int n_outputs = (int) n_outputs_t;
        typedef std::array<double_t, n_states_t> State;
        typedef std::array<double_t, n_states_t> StateDerivative;
        typedef std::array<double_t, n_actions_t> Action;
        typedef std::array<double_t, n_outputs_t> Output;
        State GetState() const;
        double_t GetStepSize() const;
        void SetState(State state);
        void SimulateStep(Action action);

    protected:
        State state{0};
        virtual StateDerivative Dynamics(State state, Action action) const = 0;
        double_t step_size = 0.001;
        
};

} /* namespace dynamic_systems */

#include "dynamic_systems/core/utils.tpp"
#include "dynamic_systems/core/dynamic_systems.tpp"
