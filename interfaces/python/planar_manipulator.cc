#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "dynamic_systems/planar_manipulator.h"

PYBIND11_MODULE(planar_manipulator, m) {
    pybind11::class_<PlanarManipulator> planar_manipulator(m, "PlanarManipulator");

    planar_manipulator.def(pybind11::init<>())
        .def("simulate_step", &PlanarManipulator::SimulateStep)
        .def("get_step_size", &PlanarManipulator::GetStepSize)
        .def("get_state", &PlanarManipulator::GetState)
        .def("set_state", &PlanarManipulator::SetState)
        .def("get_output", &PlanarManipulator::GetOutput)
        .def("get_mass_matrix", &PlanarManipulator::GetMassMatrix)
        .def_readonly("n_states", &PlanarManipulator::n_states)
        .def_readonly("n_actions", &PlanarManipulator::n_actions)
        .def_readonly("n_outputs", &PlanarManipulator::n_outputs)
        .def_readwrite("par", &PlanarManipulator::par);
    
    pybind11::class_<PlanarManipulator::Parameters> (planar_manipulator, "Parameters")
        .def(pybind11::init<>())
        .def_readwrite("mass_lever1_kg", &PlanarManipulator::Parameters::mass_lever1_kg)
        .def_readwrite("mass_lever2_kg", &PlanarManipulator::Parameters::mass_lever2_kg)
        .def_readwrite("length_lever1_m", &PlanarManipulator::Parameters::length_lever1_m)
        .def_readwrite("length_lever2_m", &PlanarManipulator::Parameters::length_lever2_m)
        .def_readwrite("acceleration_gravity_mPerSecPerSec", &PlanarManipulator::Parameters::acceleration_gravity_mPerSecPerSec)
        .def_readwrite("inertia_lever1_kgm2", &PlanarManipulator::Parameters::inertia_lever1_kgm2)
        .def_readwrite("inertia_lever2_kgm2", &PlanarManipulator::Parameters::inertia_lever2_kgm2);
}
