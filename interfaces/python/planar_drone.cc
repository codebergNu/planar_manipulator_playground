#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "dynamic_systems/planar_drone.h"

namespace py = pybind11;

PYBIND11_MODULE(planar_drone, m) {
    py::class_<PlanarDrone>(m, "PlanarDrone")
      .def(py::init<>())
      .def("simulate_step", &PlanarDrone::SimulateStep)
      .def("get_step_size", &PlanarDrone::GetStepSize)
      .def("get_state", &PlanarDrone::GetState)
      .def("set_state", &PlanarDrone::SetState)
      .def_readonly("n_states", &PlanarDrone::n_states)
      .def_readonly("n_actions", &PlanarDrone::n_actions);
}
