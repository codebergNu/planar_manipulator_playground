#include <iostream>
#include "dynamic_systems/planar_manipulator.h"
#include "dynamic_systems/dynamic_systems.h"

int main() {
    PlanarManipulator manipulator{};

    std::cout << "Number of states: " << manipulator.n_states << std::endl;
    std::cout << "Number of actions: " << manipulator.n_actions << std::endl;
    std::cout << "State: " << manipulator.GetState() << std::endl;
    PlanarManipulator::Action a0 = {10, 9};
    for (int ii = 0; ii < 10; ii++) {
        manipulator.SimulateStep(a0);
        std::cout << "Integration step: " << manipulator.GetState() << std::endl;
    }
}
